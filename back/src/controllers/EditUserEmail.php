<?php

namespace App\Controllers;

use App\Models\SqlConnect;
use PDO;
use PDOException;

class EditUserEmail extends SqlConnect {
    protected array $params;
    protected string $reqMethod;

    public function __construct($params) {
        parent::__construct();
        $this->params = $params;
        $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $this->run();
    }


    protected function postEditUserEmail($id) {
        $data = json_decode(file_get_contents("php://input"), true);
    
        if (!isset($data['email'])) {
            header('HTTP/1.1 400 Bad Request');
            echo json_encode(['message' => 'Invalid input']);
            return;
        }
    
        $email = trim($data['email']);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            header('HTTP/1.1 400 Bad Request');
            echo json_encode(['message' => 'Invalid email address']);
            return;
        }
    
        try {
            $stmt = $this->db->prepare("UPDATE users SET email = :email WHERE id = :id");
            $stmt->execute([':email' => $email, ':id' => $id]);
    
            echo json_encode([
                'success' => true,
                'message' => 'Email updated successfully',
                'data' => [
                    'email' => $email
                ]
            ]);
        } catch (PDOException $e) {
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(['message' => 'Database error', 'error' => $e->getMessage()]);
        }
    }
    
    protected function cors() {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: *");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            }

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }

            exit(0);
        }
    }

    protected function header() {
        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json; charset=utf-8');
        header("Access-Control-Allow-Headers: X-Requested-With");
    }

    protected function ifMethodExist() {
        $method = $this->reqMethod . 'EditUserEmail';

        if (method_exists($this, $method)) {
            echo json_encode($this->$method($this->params['id']));
            return;
        }

        header('HTTP/1.0 404 Not Found');
        echo json_encode([
            'code' => '404',
            'message' => 'Not Found'
        ]);
    }

    protected function run() {
        $this->cors();
        $this->header();
        $this->ifMethodExist();
    }
}
