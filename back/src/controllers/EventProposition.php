<?php

namespace App\Controllers;

use App\Models\SqlConnect;
use PDO;
use PDOException;

class EventProposition extends SqlConnect {
    protected array $params;
    protected string $reqMethod;

    public function __construct($params) {
        parent::__construct();
        $this->params = $params;
        $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $this->run();
    }

    protected function getEventProposition($id) {
        $query = "SELECT * FROM event WHERE organiser_Id != :id ORDER BY RAND() LIMIT 3";
    
        try {
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            
            $events = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
            if ($events) {
                foreach ($events as &$event) {
                    $imageBase64 = base64_encode($event['image']);
                    unset($event['image']);
                    $event['image_base64'] = $imageBase64;
                }
                return [
                    'success' => true,
                    'data' => $events
                ];
            } else {
                return [
                    'success' => false,
                    'error' => [
                        'message' => 'No events found'
                    ]
                ];
            }
        } catch(PDOException $e) {
            return [
                'success' => false,
                'error' => [
                    'message' => 'Erreur lors de la récupération des événements: ' . $e->getMessage()
                ]
            ];
        }
    }
    
    protected function cors() {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: *");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        if ($this->reqMethod === 'options') {
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
    }

    protected function header() {
        header('Content-type: application/json; charset=utf-8');
        header("Access-Control-Allow-Headers: X-Requested-With");
    }

    protected function ifMethodExist() {
        $method = $this->reqMethod . 'EventProposition';

        if (method_exists($this, $method)) {
            echo json_encode($this->$method($this->params['id']));
            return;
        }

        header('HTTP/1.0 404 Not Found');
        echo json_encode([
            'code' => '404',
            'message' => 'Not Found'
        ]);
    }

    protected function run() {
        $this->cors();
        $this->header();
        $this->ifMethodExist();
    }
}
