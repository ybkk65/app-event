<?php

namespace App\Controllers;

use App\Models\SqlConnect;
use PDO;
use PDOException;

class EditEvent extends SqlConnect {
    protected $params;
    protected $reqMethod;

    public function __construct($params) {
        parent::__construct();
        $this->params = $params;
        $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $this->run();
    }

    protected function postEditEvent($id) {
        // Récupérer les données envoyées en POST
        $postData = json_decode(file_get_contents('php://input'), true);

        // Vous pouvez accéder aux champs envoyés par Axios ainsi qu'à 'event_id' dans $postData
        $titre = $postData['titre'] ?? '';
        $description = $postData['description'] ?? '';
        $description_plus = $postData['plus-info'] ?? '';
        $categorie = $postData['categorie'] ?? '';
        // Validation des données ici si nécessaire

        // Exemple d'update SQL (à adapter selon votre structure de base de données)
        $sql = "UPDATE event SET titre = :titre, description = :description, description_plus = :description_plus, categorie = :categorie WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':titre', $titre, PDO::PARAM_STR);
        $stmt->bindParam(':description', $description, PDO::PARAM_STR);
        $stmt->bindParam(':description_plus', $description_plus, PDO::PARAM_STR);
        $stmt->bindParam(':categorie', $categorie, PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        try {
            $stmt->execute();
            // Renvoyer une réponse JSON en cas de succès
            return [
                'success' => true,
                'message' => 'Event updated successfully'
            ];
        } catch (PDOException $e) {
            // Gérer les erreurs de base de données
            return [
                'success' => false,
                'message' => 'Error updating event: ' . $e->getMessage()
            ];
        }
    }

    protected function cors() {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: *");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            }

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }

            exit(0);
        }
    }

    protected function header() {
        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json; charset=utf-8');
        header("Access-Control-Allow-Headers: X-Requested-With");
    }

    protected function ifMethodExist() {
        $method = $this->reqMethod . 'EditEvent';

        if (method_exists($this, $method)) {
            echo json_encode($this->$method($this->params['id']));
            return;
        }

        header('HTTP/1.0 404 Not Found');
        echo json_encode([
            'code' => '404',
            'message' => 'Not Found'
        ]);
    }

    protected function run() {
        $this->cors();
        $this->header();
        $this->ifMethodExist();
    }
}
