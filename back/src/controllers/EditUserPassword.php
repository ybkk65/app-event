<?php

namespace App\Controllers;

use App\Models\SqlConnect;
use PDO;
use PDOException;

class EditUserPassword extends SqlConnect {
    protected array $params;
    protected string $reqMethod;

    public function __construct($params) {
        parent::__construct();
        $this->params = $params;
        $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $this->run();
    }


    protected function postEditUserPassword($id) {
        $data = json_decode(file_get_contents("php://input"), true);

        if (!isset($data['password']) || !isset($data['confirmPassword'])) {
            header('HTTP/1.1 400 Bad Request');
            echo json_encode(['message' => 'Invalid input']);
            return;
        }

        $password = $data['password'];
        $confirmPassword = $data['confirmPassword'];

        if ($password !== $confirmPassword) {
            header('HTTP/1.1 400 Bad Request');
            echo json_encode(['message' => 'Passwords do not match']);
            return;
        }

        if (strlen($password) < 8) {
            header('HTTP/1.1 400 Bad Request');
            echo json_encode(['message' => 'Password must be at least 8 characters long']);
            return;
        }

        $hashedPassword = password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);

        try {
            $stmt = $this->db->prepare("UPDATE users SET password = :password WHERE id = :id");
            $stmt->execute([':password' => $hashedPassword, ':id' => $id]);
            echo json_encode(['message' => 'Password updated successfully']);
        } catch (PDOException $e) {
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(['message' => 'Database error', 'error' => $e->getMessage()]);
        }
    }

    protected function cors() {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: *");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            }

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }

            exit(0);
        }
    }

    protected function header() {
        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json; charset=utf-8');
        header("Access-Control-Allow-Headers: X-Requested-With");
    }

    protected function ifMethodExist() {
        $method = $this->reqMethod . 'EditUserPassword';

        if (method_exists($this, $method)) {
            echo json_encode($this->$method($this->params['id']));
            return;
        }

        header('HTTP/1.0 404 Not Found');
        echo json_encode([
            'code' => '404',
            'message' => 'Not Found'
        ]);
    }

    protected function run() {
        $this->cors();
        $this->header();
        $this->ifMethodExist();
    }
}
