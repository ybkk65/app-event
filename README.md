# EVENTSPOT - Application Web de Gestion d'Événements

## Description

EVENTSPOT est une application web permettant aux utilisateurs de gérer plusieurs événements. Voici les informations pour tester l'application :

- Email: admin@admin.com
- Mot de passe: 1234567890A@

## Fonctionnalités

- CRUD pour créer, modifier ou supprimer un événement
- Système de login et d'inscription
- Inscription et gestion des participants aux événements
- Gestion des demandes d'inscription
- Modification des informations utilisateur
- Invitation d'utilisateurs (gestion des invitations à venir)
- Vue détaillée des événements et de leurs détails
- Déconnexion

## Technologies Utilisées

- Interface Utilisateur : JavaScript, HTML, CSS
- API RESTful : PHP, MySQL

## fonctionnalite non réaliser (manque de temps)

systeme de like 
page about et contact 
page gerer les invitation


## Installation

Use the package manager [npm](https://www.npmjs.com/) to install helloworld.

Use node v20 use :
```bash
nvm install 20
```

```bash
npm i
```

## Usage

Start the application dev with :

```bash
npm run start
```

Created the dist with :

```bash
npm run dist
```

Analyse the coding rules with :

```bash
npm run lint
```


![Texte alternatif pour l'image](./images/1.png)


![Texte alternatif pour l'image](./images/2.png)


![Texte alternatif pour l'image](./images/3.png)

![Texte alternatif pour l'image](./images/a.png)


![Texte alternatif pour l'image](./images/b.png)


![Texte alternatif pour l'image](./images/c.png)



![Texte alternatif pour l'image](./images/5.png)


![Texte alternatif pour l'image](./images/6.png)


![Texte alternatif pour l'image](./images/7.png)


![Texte alternatif pour l'image](./images/8.png)


![Texte alternatif pour l'image](./images/9.png)


![Texte alternatif pour l'image](./images/10.png)


![Texte alternatif pour l'image](./images/11.png)


![Texte alternatif pour l'image](./images/12.png)


![Texte alternatif pour l'image](./images/13.png)


![Texte alternatif pour l'image](./images/14.png)


![Texte alternatif pour l'image](./images/15.png)

