export default () => (`
  <footer>
    <div class="footer-container">
      <div class="footer-column">
        <h3>About Us</h3>
        <p>Event Spot, your site for easily and<br>efficiently organizing all your special<br>occasions.</p>
      </div>
      <div class="footer-column">
        <h3>Services</h3>
        <ul>
          <li>Registration</li>
          <li>Planning</li>
          <li>Creation</li>
        </ul>
      </div>
      <div class="footer-column">
        <h3>Contact</h3>
        <ul>
          <li>Email: eventspot@gmail.com</li>
          <li>Phone: +01 23 45 67 89</li>
          <li>Address: 1 place d'arc, Orléans</li>
        </ul>
      </div>
      <div class="footer-column">
        <h3>Follow Us</h3>
        <ul class="social-icons">
          <li><a href="#"><i class="fa-brands fa-facebook"></i> Facebook</a></li>
          <li><a href="#"><i class="fa-brands fa-twitter"></i> Twitter</a></li>
          <li><a href="#"><i class="fa-brands fa-instagram"></i> Instagram</a></li>
        </ul>
      </div>
    </div>
    <div class="footer-right">
      <p>&copy; 2024 Your Company. All rights reserved.</p>
    </div>
  </footer>
`);
